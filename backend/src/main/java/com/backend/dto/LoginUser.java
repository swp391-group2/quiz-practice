package com.backend.dto;

import lombok.Data;

@Data
public class LoginUser {

    private String username;
    private String password;

}
