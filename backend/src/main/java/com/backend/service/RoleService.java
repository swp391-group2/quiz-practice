package com.backend.service;

import com.backend.entity.Role;
import com.backend.ultilities.ERole;

import java.util.Optional;

public interface RoleService {
    Optional<Role> findByName(ERole name);
}
